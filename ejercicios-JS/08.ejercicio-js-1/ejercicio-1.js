
const faker = require('faker'); 

function randomUsers(numUsers) {
    let user = {};
    let users = [];
    for (let i = 0; i < numUsers; i++) {
        user.username = faker.internet.userName();
        user.name = faker.name.firstName();
        user.surname = faker.name.lastName();
        user.gender = faker.name.gender();
        user.country = faker.address.country();
        user.email = faker.internet.email();
        user.imageUrl = faker.image.imageUrl();

        users.push(user);
    }
    return users;
}

let userList = randomUsers(2);

console.log(userList);