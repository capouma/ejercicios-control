
const initialTime = Date.now()
let i = 2

function timeChange(timeToChange) {
    let remain = timeToChange

    const days = Math.floor(remain / (1000 * 60 * 60 * 24))
    remain = remain % (1000 * 60 * 60 * 24)

    const hours = Math.floor(remain / (1000 * 60 * 60))
    remain = remain % (1000 * 60 * 60)

    const minutes = Math.floor(remain / (1000 * 60))
    remain = remain % (1000 * 60)

    const seconds = Math.floor(remain / (1000))
    remain = remain % (1000)

    return { seconds, minutes, hours, days }
}


function delay(seconds)
{
    return new Promise(  (resolve) =>
    {
        setTimeout( () =>
        {
            let timeToChange = parseInt((Date.now() - initialTime))
            let { seconds, minutes, hours, days } = timeChange(timeToChange)
            
            resolve(`Tiempo transcurrido:
                ${seconds} segundos, 
                ${minutes} minutos, 
                ${hours} horas y 
                ${days} días`)
                
        }, seconds * 1000)
    })
}

(async () =>
{
    console.log('Inicio del programa...')

    while (i !==0)
    {
        const result = await delay(5)
        console.log(result)
    }
})()

