const axios = require('axios')

function SetToArrayEpisodeCharacters(episodeCharacter)
{
    let arrayEpisodeCharacters = []
    for (character of episodeCharacter)
    {
        arrayEpisodeCharacters.push(character)
    }
    return arrayEpisodeCharacters
}

function extractCharacters(episodeCharacter)
{

    let promesas = episodeCharacter.map(promesa => axios.get(promesa))
        
    Promise.all(promesas)
        .then( data => {

            let allCharacters =[]

            for (let i = 0; i < episodeCharacter.length; i++)
            {
                allCharacters.push(data[i].data.name);
            }
            console.log(allCharacters);
        });

}

function filterEpisodes(response)
{
    const episodesFiltered = new Set()
    const listOfEpisodes = response.data.results
        .filter(listepisodes => listepisodes.air_date.includes('January'))
        .map(listepisodes => listepisodes.characters)

    for (const episodes of listOfEpisodes)
    {
        for (const episode of episodes)
        {
            episodesFiltered.add(episode);
        }

    }
        const episodeCharacters = SetToArrayEpisodeCharacters(episodesFiltered)
        extractCharacters(episodeCharacters)
    
}

axios
    .get('https://rickandmortyapi.com/api/episode')
    .then(response =>
        {
            filterEpisodes(response)

        });
        


