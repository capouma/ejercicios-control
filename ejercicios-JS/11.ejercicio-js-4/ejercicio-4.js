const names = [
    'A-Jay',
    'Manuel',
    'Manuel',
    'Eddie',
    'A-Jay',
    'Su',
    'Reean',
    'Manuel',
    'A-Jay',
    'Zacharie',
    'Zacharie',
    'Tyra',
    'Rishi',
    'Arun',
    'Kenton',
]

function cleanArrays (array)
{
    return array.filter((name, index) => array.indexOf(name) === index)
}

console.log(cleanArrays(names));