
function conversor (number, base)
{
    if (base === 2)
    {
        console.log('Convirtiendo el número introducido a binario...')
        return number.toString(2)
    }
    else if (base === 10)
    {
        console.log('Convirtiendo el número introducido a decimal...');
        return number.toString().split('').reverse().reduce(function(x, y, i)
        {
            return (y === '1') ? x + Math.pow(2, i) : x;
        }, 0);
    }
}

const numeroConvertido = conversor(1010, 10)

console.log(`El número convertido es ${numeroConvertido}`);